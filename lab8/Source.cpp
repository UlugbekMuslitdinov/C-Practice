#include <iostream>
using namespace std;

int main() {
	int choice;
	cout << "Input program number to run it: " << endl;
	cin >> choice;
	if (choice == 1) {
		int n, m, a[20][20], b[20][20];
		cout << "Input matrix dimensions: " << endl;
		cin >> n >> m;

		cout << "Input matrix A" << endl;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				cin >> a[i][j];
			}
		}

		cout << "Your matrix A is: " << endl;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				cout << a[i][j] << "\t";
			}
			cout << endl;
		}


		cout << "Input matrix B" << endl;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				cin >> b[i][j];
			}
		}

		cout << "Your matrix B is: " << endl;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				cout << b[i][j] << "\t";
			}
			cout << endl;
		}

		cout << "Sum of your matrices: " << endl;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				cout << a[i][j] + b[i][j] << "\t";
			}
			cout << endl;
		}

		cout << "Transpose of your matrices:" << endl;
		cout << "A matrix";
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				cout << a[j][i] << "\t";
			}
			cout << endl;
		}

		cout << "B matrix";
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				cout << b[j][i] << "\t";
			}
			cout << endl;
		}
		
	}


	system("pause");
	return 0;
}